using Test
using LinearAlgebra

function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
    
    if dima[2] != dimb[1]
        return -1
    end
    
    c = zeros(dima[1], dimb[2])
    
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    
    return c
end

function matrix_pot(matrix, power)
    matrix = float(matrix)
    matrix_powered = matrix

    for x in 1:power-1
        matrix_powered = multiplica(matrix_powered, matrix)
    end

    return matrix_powered
end

function is_even(integer)
    return integer % 2 == 0
end

function is_odd(integer)
    return integer % 2 != 0
end

function matrix_pot_by_squaring(matrix, p)

    if p == 1
        return matrix
    
    elseif is_even(p)
        return matrix_pot_by_squaring(matrix_pot(matrix, 2), p/2)
    
    else
        return multiplica(matrix, matrix_pot_by_squaring(matrix_pot(matrix, 2), (p - 1) / 2))
    end
end

function newline()
    print("\n")
end

@testset "check multiply" begin
    @test multiplica([7 1; 2 9],  [3 4; 6 -5]) == [27 23; 60 -37]
    @test multiplica([-6 2; 4 5], [4 9; 1 -3]) == [-22 -60; 21 21]
    @test multiplica([1 4; 3 3], [1 4; 3 3]) == [13 16; 12 21]
    @test multiplica([2 3; 1 0; 4 5], [3 1; 2 4]) == [12 14; 3 1; 22 24]
end

@testset "check matrix_pot" begin
    @test matrix_pot([1 2 ; 3 4], 2) == [7 10; 15 22]
    @test matrix_pot([1 2 ; 3 4], 2) == [7 10; 15 22] 
    @test matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7) ≈ [5.6428432e8 4.70888932e8 3.23583236e8 3.51858636e8;
                                                                    8.31242352e8 6.93529366e8 4.76618654e8 5.18192434e8; 
                                                                    5.77003992e8 4.81472568e8 3.30793288e8 3.59676984e8;
                                                                    7.99037372e8 6.66708057e8 4.58121425e8 4.98126827e8]
end

@testset "check matrix_pot_by_squaring" begin
    @test matrix_pot_by_squaring([1 2 ; 3 4], 2) == [7 10; 15 22]
    @test matrix_pot_by_squaring([1 2 ; 3 4], 2) == [ 7 10 ; 15 22 ] 
    @test matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7) ≈ [5.6428432e8 4.70888932e8 3.23583236e8 3.51858636e8;
                                                                    8.31242352e8 6.93529366e8 4.76618654e8 5.18192434e8; 
                                                                    5.77003992e8 4.81472568e8 3.30793288e8 3.59676984e8;
                                                                    7.99037372e8 6.66708057e8 4.58121425e8 4.98126827e8]   
end

function compare_times()
    matrix_10 = Matrix(LinearAlgebra.I, 10, 10)
    matrix_20 = Matrix(LinearAlgebra.I, 20, 20)
    matrix_30 = Matrix(LinearAlgebra.I, 30, 30)

    newline()
    @time matrix_pot(matrix_10, 10)
    @time matrix_pot_by_squaring(matrix_10, 10)
    
    newline()
    @time matrix_pot(matrix_20, 10)
    @time matrix_pot_by_squaring(matrix_20, 10)
    
    newline()
    @time matrix_pot(matrix_30, 10)
    @time matrix_pot_by_squaring(matrix_30, 10)
end

compare_times()